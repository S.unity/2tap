# 2tap

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
### server
```
npx http-server dist
```
### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Doc. PWA 
https://webdevblog.ru/sozdanie-pwa-s-pomoshhju-vue-js/
