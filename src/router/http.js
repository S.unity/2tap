import Vue from 'vue'
import axios from 'axios'
import Loading from 'vue-loading-overlay'
import 'vue-loading-overlay/dist/vue-loading.css'
Vue.use(Loading)

// create a new axios instance
const instance = axios.create({
  baseURL: '/api'
})

let loader
// before a request is made start the nprogress
instance.interceptors.request.use(config => {
  if (loader !== undefined) loader.hide()
  loader = Vue.$loading.show({
    loader: 'dots',
    opacity: 0.8,
    backgroundColor: '#000',
    color: '#FA5663',
    isFullPage: true
  })
  return config
})

// before a response is returned stop nprogress
instance.interceptors.response.use(response => {
  loader.hide()
  return response
})

export default instance
