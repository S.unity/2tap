import Vue from 'vue'
import VueRouter from 'vue-router'
import VueLoading from 'vue-loading-overlay'
import 'vue-loading-overlay/dist/vue-loading.css'
import Home from '../views/Home.vue'


Vue.use(VueLoading)
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/auth/:id',
    name: 'auth',
    component: () => import(/* webpackChunkName: "about" */ '../views/Auth'),
    params: true
  },
  {
    path: '/feedback',
    name: 'feedback',
    component: () => import(/* webpackChunkName: "about" */ '../views/Feedback'),
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '../views/About'),
  },
  {
    path: '/faq',
    name: 'faq',
    component: () => import(/* webpackChunkName: "about" */ '../views/FAQ'),
  },
  {
    path: '/profile/:id',
    name: 'profile',
    component: () => import(/* webpackChunkName: "about" */ '../views/Profile'),
    params: true
  },
  {
    path: '/my-profile',
    name: 'my-profile',
    component: () => import(/* webpackChunkName: "about" */ '../views/My-profile'),
  },
  {
    path: '/account/:id',
    name: 'account',
    component: () => import(/* webpackChunkName: "about" */ '../views/Account'),
    params: true
  },
  {
    path: '/favorites',
    name: 'favorites',
    component: () => import(/* webpackChunkName: "about" */ '../views/Favorites')
  },
  {
    path: '/settings',
    name: 'settings',
    component: () => import(/* webpackChunkName: "about" */ '../components/Settings')
  },
  {
    path: '/change-password',
    name: 'change-password',
    component: () => import(/* webpackChunkName: "about" */ '../components/Change-password')
  },
  {
    path: '/place-ad',
    name: 'place-ad',
    component: () => import(/* webpackChunkName: "about" */ '../components/Place-ad')
  },
  {
    path: '/edit-ad',
    name: 'edit-ad',
    component: () => import(/* webpackChunkName: "about" */ '../views/Edit-ad')
  },
  {
    path: '/notice',
    name: 'notice',
    component: () => import(/* webpackChunkName: "about" */ '../views/Notice')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return {selector: to.hash}
    } else if (savedPosition) {
      return savedPosition
    } else {
      return {x: 0, y: 0}
    }
  }
})

let loader
router.beforeEach((to, from, next) => {
  // If this isn't an initial page load.
  if (to.name) {
    // Start the route progress bar.
    loader = Vue.$loading.show({
      loader: 'dots',
      opacity: 0.8,
      backgroundColor: '#000',
      color: '#FA5663',
      isFullPage: true
    })
  }
  next()
})

router.afterEach((to, from) => {
  loader.hide()
})
export default router
